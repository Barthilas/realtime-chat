import React, {useState, useEffect} from 'react';
import queryString from 'query-string';
import io from 'socket.io-client';

import './Chat.css';

import InfoBar from '../InfoBar/InfoBar';
import Input from '../Input/Input';
import Messages from '../Messages/Messages';
import UsersBar from '../UsersBar/UsersBar';
import PrivateMessages from '../PrivateMessages/PrivateMessages';

let socket;
const ENDPOINT = process.env.REACT_APP_API_KEY;
const Chat = ({ location }) => {
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [users, setUsers] = useState('');
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    const [privateMessage, setPrivateMessage] = useState('');
    const [privateMessages, setPrivateMessages] = useState([]);
    const [privateMessageTo, setPrivateMessageTo] = useState('')

    //fire every time location.search changes
    useEffect(() => {
      const { name, room } = queryString.parse(location.search);
      /* name = location.state.username */
      socket = io(ENDPOINT);
      setRoom(room);
      setName(name)
  
      socket.emit('join', { name, room }, (error) => {
        if(error) {
          alert(error);
        }
      });
    }, [location.search]);
    
    useEffect(() => {
      socket.on('message', message => {
        setMessages(messages => [ ...messages, message ]);
      });
      
      socket.on("roomData", ({ users }) => {
        setUsers(users);
      });

      socket.on('privateMessage', privateMessage => {
        setPrivateMessages(privateMessages => [ ...privateMessages, privateMessage ]);
      });

    }, []);
  
    const sendMessage = (event) => {
      event.preventDefault();
  
      if(message) {
        socket.emit('sendMessage', message, () => setMessage(''));
      }
    }

    function sendPrivateMessage(event){
      let to = privateMessageTo
      event.preventDefault()
      if(privateMessage)
      {
        socket.emit('sendPrivateMessage', {to, privateMessage}, () => setPrivateMessage(''));
      }
    }
    
    //console.log(message, messages)
    console.log(privateMessage, privateMessages)
    
    return (
      <div className="outerContainer">
        <div className="container1">
            <InfoBar room={room} />
            <Messages messages={messages} name={name} />
            <Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
        </div>
        {<UsersBar users={users} privateMessageTo={setPrivateMessageTo}/>}
        <div className="container1">
            <InfoBar room={privateMessageTo} />
            <PrivateMessages messages={privateMessages} from={name} to={privateMessageTo}></PrivateMessages>
            <Input message={privateMessage} setMessage={setPrivateMessage} sendMessage={sendPrivateMessage}/>
        </div>
      </div>
    );
  }
  
  export default Chat;