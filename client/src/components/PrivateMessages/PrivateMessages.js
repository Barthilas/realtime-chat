import React from 'react';

import ScrollToBottom from 'react-scroll-to-bottom';

import './PrivateMessages.css';
import Message from '../Message/Message';

const PrivateMessages = ({messages, from, to}) => {
return(
<ScrollToBottom className="messages">
    {messages.map((message, i) => {
        if(to === message.user || from === message.user)
        {
            return <div key={i}><Message message={message} name={from}/></div>
        }
      })}
</ScrollToBottom>
)
}

export default PrivateMessages;