module.exports = {
    // 1. MongoDB
    MONGO_URI: process.env.MONGO_URI || 'mongodb://localhost/apijwt',
  
    // 2. JWT
    TOKEN_SECRET: process.env.TOKEN_SECRET || 'vBwZrzfmR6FekY7WuNMEZJEkOzxgGW2CBwYiMghqKRP4ZuJ2zamSsJG5KUpnhp9',
  
    // 3. Express Server Port
    LISTEN_PORT: process.env.PORT || 5000
  };