import React, { useState, useEffect } from 'react';
import Axios from "axios";
import { Link } from 'react-router-dom';
import { useHistory } from "react-router"
import './Register.css';

const Register = () => {

    const [registerEmail, setRegisterEmail] = useState("");
    const [registerUsername, setRegisterUsername] = useState("");
    const [registerPassword, setRegisterPassword] = useState("");
    const [redirectReady, setRedirectReady] = useState(false)
    let history = useHistory();

    useEffect(() => {
        if (redirectReady) {
            history.push("/login");
        }
    }, [redirectReady]);

    const registerPostAPI = async () => {
        Axios({
            method: "POST",
            data: {
              name: registerUsername,
              email: registerEmail,
              password: registerPassword,
            },
            withCredentials: true,
            url: process.env.REACT_APP_API_KEY + "api/user/register",
          }).then((res) => {
            console.log(res)
            setRedirectReady(true)
          }
          ).catch(err => {
            console.log(err.response);
            window.alert(err.response.data);
          });
    };

    const handleSubmit = async(event) => {
        event.preventDefault();
        await registerPostAPI()
    }

return(

<div className="login-form">
    <form onSubmit={handleSubmit}>
        <h2 className="text-center">Register</h2>       
        <div className="form-group">
            <input type="text" className="form-control" placeholder="Username" required="required" onChange={(e) => setRegisterUsername(e.target.value)}/>
        </div>
        <div className="form-group">
            <input type="email" className="form-control" placeholder="Email" required="required" onChange={(e) => setRegisterEmail(e.target.value)}/>
        </div>
        <div className="form-group">
            <input type="password" className="form-control" placeholder="Password" required="required" onChange={(e) => setRegisterPassword(e.target.value)}/>
        </div>
        <div className="form-group">
            <button type="submit" className="btn btn-primary btn-block">Register account</button>
        </div>    
    </form>
    <Link to={`/login`}>
        <p className="text-center">Login</p>
    </Link>
</div>
);
}
export default Register;