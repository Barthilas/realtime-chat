const express = require('express');
const socketio = require('socket.io');
const http = require('http');
const bodyParser = require('body-parser');
const mongoose = require("mongoose");
const cors = require("cors");
const config = require('./config');
const dotenv = require('dotenv');

const {addUser, removeUser, getUser, getUsersInRoom, getUserId} = require('./users.js')

//Initialize the application 
dotenv.config();
const app = express();
//app.use(cors()); //Jesus..
app.use(cors({credentials: true, origin: 'http://localhost:3000'}));
const server = http.createServer(app); 
const io = socketio(server);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Import routes
const router = require('./routes/router');
const authRoute = require('./routes/auth');
const privateRoute = require('./routes/privateRoutes')

//Force https in production
if (app.get('env') === 'production') {
    app.use(function(req, res, next) {
      var protocol = req.get('x-forwarded-proto');
      protocol == 'https' ? next() : res.redirect('https://' + req.hostname + req.url);
    });
  }

io.on('connection', (socket) => {  //or connect
    console.log('We have a new connection.');

    socket.on('join', ({name, room}, callback) => {
        const {error , user} = addUser({id: socket.id, name, room});
        console.log("User: " + name + " Room: " + room)
        
        if(error) return callback(error)

        socket.emit('message', {user: 'admin', text: `${user.name}, welcome to the room ${user.room}.`});
        socket.broadcast.to(user.room).emit('message', {user: 'admin', text: `${user.name} has joined!`})

        socket.join(user.room);

        io.to(user.room).emit('roomData', {room: user.room, users: getUsersInRoom(user.room)});

        callback();
    });

    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id);

        io.to(user.room).emit('message' , {user: user.name, text: message});
        io.to(user.room).emit('roomData' , {room: user.room, users: getUsersInRoom(user.room)});
        callback();
    });

    socket.on('sendPrivateMessage', ({to, privateMessage}, callback) => {
        const sender = getUser(socket.id)
        const toUser = getUserId(to);
        if(toUser)
        {
            if(sender != toUser) /* dont sent to self two times. */
            {
                io.to(sender.id).emit('privateMessage', {user: sender.name, text: privateMessage});
            }
            io.to(toUser.id).emit('privateMessage', {user: sender.name, text: privateMessage});
        }
        callback();
    });


    socket.on('disconnect', () => {
        console.log('User had left.');
        const user = removeUser(socket.id);

        if(user){
            io.to(user.room).emit('message', {user: 'admin', text: `${user.name} has left.`});
            io.to(user.room).emit('roomData' , {room: user.room, users: getUsersInRoom(user.room)});
        }
    });
});
// 5. Connect to MongoDB Atlas
mongoose.connect(process.env.DB_CONNECT,  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    },
() => console.log("DB connected."));

//Middleware
app.use(express.json());

//Route Middlewares
app.use('/api/user', authRoute);
app.use(router);
app.use('/api/private', privateRoute);
 
//Start the server
server.listen(config.LISTEN_PORT, () => console.log("Server has started on port %s", config.LISTEN_PORT));
