const router = require('express').Router();
const verify = require('./verifyToken');
const User = require('../model/User');

//api/private/...
router.get('/', verify, async (req, res) => {
    //res.send(req.user);
    const user = await User.findOne({_id: req.user});
    //console.log(user)
    res.send({username: user.name})
    //res.json({post: {title: 'test', description: 'secure data'}});
});

module.exports = router;