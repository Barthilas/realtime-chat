import React from 'react'

import { BrowserRouter as Router, Route} from 'react-router-dom'
import { Redirect } from "react-router-dom"; 

import Join from './components/Join/Join'
import Chat from './components/Chat/Chat'
import Login from './components/Login/Login'
import Register from './components/Register/Register';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        localStorage.getItem("auth-token")
        ? <Component {...props} />
        : <Redirect to='/login' />
    )} />
  )

const App = () => (
<Router>
    <PrivateRoute path="/" exact component={Join} />
    <Route path="/register" component={Register}></Route>
    {/* <Route path="/chat" component={Chat} /> */}
    <Route path="/login" component={Login}></Route>
    <PrivateRoute path="/chat" component={Chat}></PrivateRoute>
</Router>
);

export default App;