import React from 'react';

import onlineIcon from '../../icons/onlineIcon.png';
import './UsersBar.css';



const UsersBar = ({ users, privateMessageTo }) => (
    <div className="textContainer">
      {
        users
          ? (
            <div>
              <h1>People currently chatting:</h1>
              <div className="activeContainer">
                <h2>
                  {users.map(({name}) => (
                    <button key={name} className="activeItem" onClick={e => privateMessageTo(e.target.innerText)}>
                      {name}
                      <img alt="Online Icon" src={onlineIcon}/>
                    </button>
                  ))}
                </h2>
              </div>
            </div>
          )
          : null
      }
    </div>
  );

export default UsersBar;