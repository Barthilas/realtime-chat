import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import "./Login.css";
import Axios from "axios";
import { useHistory } from "react-router"

function Login() {
  let history = useHistory()
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const [redirectReady, setRedirectReady] = useState(false)

  useEffect(() => {
    if (redirectReady) {
        history.push("/");
    }
}, [redirectReady]);

  const loginPostAPI = () => {
    Axios({
      method: "POST",
      data: {
        email: loginEmail,
        password: loginPassword,
      },
      withCredentials: true,
      url: process.env.REACT_APP_API_KEY + "api/user/login",
    }).then((res) => {
        console.log(res)
        localStorage.setItem("auth-token", res.data) //not a good idea but for test works, other option could be cookie
        setRedirectReady(true);
      }
    ).catch(err => {
      console.log(err.response);
      window.alert(err.response.data);
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    loginPostAPI();
  };
  return (
    <div className="login-form">
    <form onSubmit={handleSubmit}>
        <h2 className="text-center">Login</h2>       
        <div className="form-group">
            <input type="email" className="form-control" placeholder="Email" required="required" onChange={(e) => setLoginEmail(e.target.value)}/>
        </div>
        <div className="form-group">
            <input type="password" className="form-control" placeholder="Password" required="required" onChange={(e) => setLoginPassword(e.target.value)}/>
        </div>
        <div className="form-group">
            <button type="submit" className="btn btn-primary btn-block">Login</button>
        </div>    
    </form>
    <Link to={`/register`}>
        <p className="text-center">Register</p>
    </Link>
</div>
  );
}

export default Login;