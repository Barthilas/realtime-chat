import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import Axios from "axios";
 //Maybe change the parameters to props.
import './Join.css';

const Join = () => {
    const[name, setName] = useState('');
    const [room, setRoom] = useState('');

    useEffect(() => {
        //on load-prep username
        getUser()
      }, []);

    const getUser = () => {
        Axios({
        headers: {'auth-token': localStorage.getItem("auth-token")}, //send back the token from localstorage
          method: "GET",
          withCredentials: true,
          url: process.env.REACT_APP_API_KEY + "api/private",
        }).then((res) => {
            console.log(res.data.username);
          setName(res.data.username);
        }).catch(err => {
            console.log(err.response);
            window.alert(err.response.data);
        });
      };

    const joinRoom = (event) => {
        if(!name || !room)
        {
            event.preventDefault();
            alert("error parameters.")
        }
    };
    return(
        <div className="joinOuterContainer">
            <div className="joinInnerContainer">
                <h1 className="heading">Join</h1>
                {/* <div><input placeholder="Name" className="joinInput" type="text" onChange={(event) => setName(event.target.value)}></input></div> */}
                <div><input placeholder="Room" className="joinInput mt-20" type="text" onChange={(event) => setRoom(event.target.value)}></input></div>
                {/* onClick={event => (!name || !room ? event.preventDefault() : null)} */}
                <Link onClick={joinRoom} to={`/chat?name=${name}&room=${room}`}>
                    <button className="button mt-20" type="submit">Join</button>
                </Link>

                {/* <Link 
                onClick={joinRoom}
                to={{
                    pathname: `/chat`,
                    search: `?room=${room}`,
                    state: {username:name}
                }}
                >
                <button className="button mt-20" type="submit">Join</button>
                </Link>  */}

                <button className="button mt-20" type="submit" onClick={() => {alert(name)}}>Who am I?</button>
            </div>
        </div>
    )
}

export default Join;